#define _POSIX_C_SOURCE 199309L
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <time.h>

#include "Tokimec.h"

uint64_t diff(struct timespec t1, struct timespec t2);

int main(int argc, char *argv[])
{
	// Make sure the user specifies a port
	if (argc != 2) {
		puts("Please specify a serial port to read from.\nUsage: tokimec_process SERIAL_PORT");
		return EXIT_FAILURE;
	}

	// Open the specified serial port.
	int fd = open(argv[1], O_RDWR | O_NOCTTY | O_NDELAY);
	// If opening fails, error out.
	if (fd == -1) {
		char x[128];
		sprintf(x, "Failed to open port: '%s'.\n", argv[1]);
		perror(x);
		return EXIT_FAILURE;
	}
	// Otherwise set port options.
	else {
		// Set reads to be non-blocking.
		fcntl(fd, F_SETFL, 0);

		// And set up a myriad of serial port options
		struct termios options;
		tcgetattr(fd, &options); // Pull the current options for a read-modify-write configuration.
		// Set the input & output baud rate to 115200
		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);

		// Disable all input and output processing	
		options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
				    INLCR | PARMRK | INPCK | ISTRIP | IXON);
		options.c_oflag = 0;

		// Disable line processing
		options.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
		
		// Disable character processing
		options.c_cflag &= ~(CSIZE | PARENB);
		options.c_cflag |= CS8;

		// Set read() to timeout immediately and return 1 character minimum.
		options.c_cc[VMIN]  = 1;
		options.c_cc[VTIME] = 0;

		// Enable the receiver and set local mode.
		options.c_cflag |= CLOCAL | CREAD;
		
		// Finally write these new settings for the port.
		if (tcsetattr(fd, TCSANOW, &options) < 0) {
			perror("Failed to configure port.");
			return EXIT_FAILURE;
		}

		// Set reads to be non-blocking on stdin as well. Necessary to allow for user input.
		tcgetattr(STDIN_FILENO, &options);
		options.c_lflag &= ~ICANON;
		options.c_cc[VMIN] = 0; // Specify that no characters may be read by calls to read().
		options.c_cc[VTIME] = 0; // Set timeout for a read() to be immediate.	
	
		// Finally write these new settings for the port.
		if (tcsetattr(STDIN_FILENO, TCSANOW, &options) < 0) {
			perror("Failed to configure stdin for non-blocking reads.");
			return EXIT_FAILURE;
		}
	}

	puts("\033[2J");

	char buffer[1024];
	TokimecOutput o;
	struct timespec lastUpdateTime, lastOutputTime, newUpdateTime;
	bool quit = false;
	while (!quit) {
		// Process any new user input. We wait for a 'c' character to start calibration.
		// An 'e' character will end calibration.
		char c;
		int userInput = read(STDIN_FILENO, &c, sizeof(c)); 		
		if (userInput) {
                    char outBuf[10];
                    TokimecPackageCommandMessage(outBuf, 1 << (c - 'a'), 5);
                    write(fd, outBuf, sizeof(outBuf));
                    switch (c) {
                        case 'c': {
                            // Enable calibration. Also set alignment time to 5s.
                            char outBuf[10];
                            TokimecPackageCommandMessage(outBuf, TOKIMEC_COMMAND_CALIBRATE_COMPASS, 5);
                            write(fd, outBuf, sizeof(outBuf));
                            printf("Starting calibration...\n");
                        } break;
                        case 'r': {
                            // When calibration is finished, perform a software reset to load the new calibration values.
                            char outBuf[10];
                            TokimecPackageCommandMessage(outBuf, TOKIMEC_COMMAND_RESTART, 5);
                            write(fd, outBuf, sizeof(outBuf));
                            printf("Resetting...\n");
                        } break;
			case 'q':
				quit = true;
				break;
                    }
		}

		// Process any new serial input
		int numBytes = read(fd, buffer, sizeof(buffer));
		if (numBytes) {
			for (int i = 0; i < numBytes; ++i) {
				int result = TokimecParse(buffer[i], &o);
				if (result == 1) {
					// Only print the newest data if at least .1s have passed since the last time we output data. This makes things more readable.
					clock_gettime(CLOCK_REALTIME, &newUpdateTime);
					if (diff(lastOutputTime, newUpdateTime) > 1e8) {
						// Print the current Tokimec output, clearing the screen first.
						puts("\033[2J\033[H");
						puts("============================================");
						TokimecPrint(&o);
						printf("Period: %.3fs\n", diff(lastUpdateTime,newUpdateTime)/1e9);
						clock_gettime(CLOCK_REALTIME, &lastOutputTime);
					}
					clock_gettime(CLOCK_REALTIME, &lastUpdateTime);
				} else if (result == -1) {
					puts("\033[H");
					puts("========Checksum calculation failed.========");
				} else if (result == -2) {
					puts("\033[H");
					puts("============Didn't find a footer.===========");
				}
			}
		}
	}	

	// Finally close the serial port.
	close(fd);

	// And return success.
	return EXIT_SUCCESS;
}

/**
 * Calculate t2 - t1 and return that value in nanoseconds.
 */
uint64_t diff(struct timespec t1, struct timespec t2)
{
	uint64_t timeDiff;
	if ((t2.tv_nsec - t1.tv_nsec) < 0) {
		timeDiff = 1000000000 + t2.tv_nsec - t1.tv_nsec;
	} else {
		timeDiff = t2.tv_nsec - t1.tv_nsec;
	}
	return timeDiff;
}

