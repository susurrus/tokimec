tokimec
=======

Tokimec VSAS-2GM command line data parser. Tokimec.h/c has been written with embedded systems in mind and is removable from the command-line interface.

## Requirements:
 1. POSIX termios.h header in the standard library
 2. ANSI-compliant terminal emulator (realterm, xterm, iTerm, etc.)

## Usage
 1. Run `make`
 2. Run the program with its only argument the name of the serial port to connect to.
   * Pressing 'r' will issue a software reset command to the IMU. 
   * Pressing 'c' will enter calibration mode for the magnetometers. Rotate at <= 6deg/s until "Compass calibration" shows that it's complete. Press 'r' to issue a software reset and load the new calibration.
   * Pressing 'q' will quit. Also CTRL+c should work.
